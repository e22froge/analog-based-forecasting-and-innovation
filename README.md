# Analog-Based Forecasting: A Simplified Example

## Overview

This repository provides a minimal implementation of an analog forecasting method using nearest-neighbor search. While the associated article applies this technique to turbulent velocity data, this code illustrates its principles on a Lorenz system for clarity and accessibility.

## Reference

For a detailed application to turbulence forecasting and a full methodological presentation, see:

> E. Frogé, C. Granero-Belinchón, S. G. Roux, N. B. Garnier, and T. Chonavel. *Analog-Based Forecasting of Turbulent Velocity: Relationship Between Unpredictability and Intermittency*. Europhysics Letters, vol. 149, no. 1, p. 13001, 2025. DOI: 10.1209/0295-5075/ada4d3.

## Usage

Run the script to generate a Lorenz time series, perform analog forecasting, and visualize the results:

```bash
python Example_Lorenz.py
```

## Files

- `Example_Lorenz.py`: Runs the forecasting method on a Lorenz system.
- `Compute_Innovation.py`: Implements database creation and nearest-neighbor regression.

## Citation

If you use this code, please cite:

> E. Frogé, C. Granero-Belinchón, S. G. Roux, N. B. Garnier, and T. Chonavel. *Analog-Based Forecasting of Turbulent Velocity: Relationship Between Unpredictability and Intermittency*. Europhysics Letters, vol. 149, no. 1, p. 13001, 2025. DOI: 10.1209/0295-5075/ada4d3.
